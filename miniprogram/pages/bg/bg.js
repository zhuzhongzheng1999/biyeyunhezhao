// pages/bg/bg.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    bgImg: [],
    pe: '',
    img: ''
  },

  // 获取所有的信息
  get_bg: async function () {
    const db = wx.cloud.database()
    var res = await db.collection('bg').limit(100).get()
    console.log(res.data)
    this.setData({
      bgImg: res.data
    })
    wx.hideLoading()
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.get_bg()
    img: ""
    this.setData({
      pe: options.pe
    })
  },


  radioChange: function(e) {
    console.log(e)
    this.setData({
      img: e.detail.value
    })
  },

  next: function() {
    if (this.data.img == '') {
      wx.showToast({
        title: '请选择背景',
        icon: 'none'
      })
      return;
    }
    wx.redirectTo({
      url: "/pages/canv/canv?bg=" + this.data.img + "&pe=" + this.data.pe
    })
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    wx.showLoading({
      title: '正在加载资源',
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },
  

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})