// miniprogram/pages/zhenren/zhenren.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    people: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  // 上传图片
  doUpload: function () {
    var that = this;
    // 选择图片
    wx.chooseImage({
      count: 1,
      sizeType: ['compressed'],
      sourceType: ['album', 'camera'],
      success: function (res) {

        wx.showLoading({
          title: '上传中',
        })

        const filePath = res.tempFilePaths[0]

        // 上传图片
        const cloudPath = 'my-image' + filePath.match(/\.[^.]+?$/)[0]
        wx.cloud.uploadFile({
          cloudPath,
          filePath,
          success: res => {
            console.log('[上传文件] 成功：', res)
            console.log("filepath", filePath)
            console.log("cloudpat", cloudPath)
            console.log("file", res.fileID)
            that.get_link(res.fileID)
            app.globalData.fileID = res.fileID
            app.globalData.cloudPath = cloudPath
            app.globalData.imagePath = filePath
            console.log("filepath",filePath)
            console.log("cloudpat",cloudPath)
            console.log("file",res.fileID)
            wx.navigateTo({
              url: '../storageConsole/storageConsole'
            })
          },
          fail: e => {
            console.error('[上传文件] 失败：', e)
            wx.showToast({
              icon: 'none',
              title: '上传失败',
            })
          },
          complete: () => {
            wx.hideLoading()
          }
        })

      },
      fail: e => {
        console.error(e)
      }
    })
  },

  // 获取临时文件网络地址链接
  get_link: function (file) {
    var that = this;
    wx.cloud.getTempFileURL({
      fileList: [file],
      success: res => {
        // fileList 是一个有如下结构的对象数组
        // [{
        //    fileID: 'cloud://xxx.png', // 文件 ID
        //    tempFileURL: '', // 临时文件网络链接
        //    maxAge: 120 * 60 * 1000, // 有效期
        // }]
        console.log(res.fileList[0].tempFileURL)
        that.fenge(res.fileList[0].tempFileURL)
      },
      fail: console.error
    })
  },

  // 分割图片
  fenge: function (url) {
    var that = this;
    console.log("ss")
    wx.serviceMarket.invokeService({
      service: 'wx2d1fd8562c42cebb',
      api: 'segmentPortraitPic',
      data: {
        "Action": "SegmentPortraitPic",
        "Url": url
      },
    }).then(res => {
      console.log("成功！")
      console.log('invokeService success', res)
      that.setData({
        people: res.data.ResultImageUrl
      })
    }).catch(err => {
      console.log("失败！")
      console.error('invokeService fail', err)
    })
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})