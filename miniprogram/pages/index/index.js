//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    bgHeight: ''
  },
  onLoad: function () {
    var that = this;
    wx.getSystemInfo({
      success: function(res) {
        console.log(res.statusBarHeight)
        that.setData({
          bgHeight: res.screenHeight - res.statusBarHeight
        })
      },
    })
  },
  go_people: function () {
    wx.navigateTo({
      url: '/pages/people/people',
    })
  }
})
